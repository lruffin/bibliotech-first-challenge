var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/', function(req, res, next) {
  console.log("Body starts here:")
  console.log(req.body);
  console.log("...End Body")
  res.send(`Hi ${req.body.firstName} ${req.body.lastName}`)
});
module.exports = router;
