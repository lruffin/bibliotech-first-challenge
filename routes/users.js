var express = require('express');
var router = express.Router();
var Sequelize = require('sequelize');

/* GET users listing. */
router.get('/', function(req, res, next) {
  const db = require('../models/index');
  const User = require('../models/user')(db.sequelize, db.Sequelize);
  
  User.sync({force: false}).then(() => { // force: true will drop the table if it already exists
    // Table created
    return User.create({
      email: 'john@hancock.org',
      password: 'JaneAndJohn'
    });
  }).then((result) => {
    User.findAll().then(users => {
      res.send(users);
    })
  });
});

router.post('/signup', function(req, res, next) {
  var username = req.body.username;
  var password = req.body.password;
  var confirmPassword = req.body.confirmPassword;
  console.log(`Registration attempt: ${username} ${password} ${confirmPassword}`);
  res.send();
});

router.post('/signin', function(req, res, next) {
  var username = req.body.username;
  var password = req.body.password;
  console.log(`Login attempt: ${username} ${password}`);
  res.send();
});

module.exports = router;
