module.exports = {
  development: {
    username: "username",
    password: "password",
    database: "database",
    dialect: "sqlite",
    storage: "../database.sqlite"
  }
};