# Bibliotech First Challange

## FE

### my-view1: signup form

- [x] email, password and confirm password
- [x] run client side validation for email and passwords (only formal)
- [ ] run client side check for same passwords
- [x] POST /users/signup

### my-view2: login form

- [x] email, password
- [x] run client side validation for email and passwords
- [x] POST /users/signin

#### Resources

- https://www.webcomponents.org/element/PolymerElements/iron-form
- https://www.webcomponents.org/element/PolymerElements/paper-input

## SERVER

- [x] Create express.js server
- [x] host the polymer app
see https://stackoverflow.com/questions/42702156/serving-polymer-app-routed-pages-with-express-js
https://stackoverflow.com/questions/42702156/serving-polymer-app-routed-pages-with-express-js and
https://expressjs.com/en/starter/static-files.html](https://expressjs.com/en/starter/static-files.html
- [x] Create a sequelize (http://docs.sequelizejs.com/) user model with an email and password 

## BE
- [ ] Authenticate each of the forms using passport-local and express session against the user model https://github.com/jaredhanson/passport-local and https://github.com/expressjs/session (see https://www.airpair.com/express/posts/expressjs-and-passportjs-sessions-deep-dive)
- [x] Create POST /users/signup
- [x] Create POST /users/signin
- [ ] Create a get route on the server at /users to pass back the current logged in user (req.user) to the client
- [ ] Write tests for the express routes using mocha and supertest (https://www.npmjs.com/package/supertest) to make the API calls